### The Grand Unified Load Balancer
A solution to manage multiple microservices in a path based routing structure.

The principle is as follows:
 1. Create 1 ALB, With 1 SG and 1 Listener
 2. Use this as your central web ingress (apply SG, WAF acl etc.)
 3. Let every app in your landscape register a rule for their path ath the `GulbListenerArn`

This makes managing public microservice urls in path based routing a breeze.

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Fgulb%2FREADME.md&dt=gulb)
