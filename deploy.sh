#!/bin/bash

# Infra requirements:
# CI/CD User in the account
# Cloudformation role in the account
# "Application / Backend" Subnets with a Name tag (name to be placed in: $AppSubnetName)

# Vars:
# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
# AWS_DEFAULT_REGION
# PubSubnetName
# StackName
# InstanceType
# AdminSecurityGroups
# Vpc
# LbSecurityGroups

# Get the cloudformation arn, this is a required var for ansible to be able to switch role and deploy stacks.
accountId=$(aws sts get-caller-identity | jq '.Account' -r)
CloudFormationRoleArn="arn:aws:iam::$accountId:role/Cloudformation"

PublicSubnets=$(aws ec2 describe-subnets --filters Name=tag:Name,Values=$PubSubnetName --output json | jq '[.Subnets[] .SubnetId] | @csv' -r | sed 's/","/,/g')

EXIT=0

echo "Deploy GULB"
vars_json='{"CloudFormationRoleArn": "'$CloudFormationRoleArn'", "LoadBalancerSubnets": '$PublicSubnets'}'
echo "vars_json:"
echo $vars_json
ansible-playbook gulb-ansible.yml --extra-vars "$vars_json" -vvvvv >> gulb-ansible.log
EXIT=$(($EXIT + $?))
echo "$EXIT"
